import java.util.Scanner;

public class Kondisional {
    
    public static void main(String[] args)
    {
        String reload = "";

        // Using loop
        do {
            System.out.println("=========================================");
            System.out.println("========= Kalkulator Nilai ==============");
            System.out.println("=========================================");
            Scanner inputdata = new Scanner(System.in);
            System.out.println("========  INPUT NILAI MAHASISWA  ========");
            System.out.print("Masukkan Nama         : ");
            String Nama = inputdata.nextLine();
            System.out.print("Masukkan NIM          : ");
            String NIM = inputdata.nextLine();
            System.out.print("Masukkan Nilai Tugas  : ");
            double nTugas = inputdata.nextDouble();
            System.out.print("Masukkan Nilai UTS    : ");
            double nUTS = inputdata.nextDouble();
            System.out.print("Masukkan Nilai UAS    : ");
            double nUAS = inputdata.nextDouble();
            System.out.println("=========================================");

            Nilai mhs2 = new Nilai(Nama, NIM, nTugas, nUTS, nUAS); // Using Overloaded Constructor

            mhs2.hitungNilai();
            mhs2.cetakNilai();

            System.out.print("Lakukan Input Ulang Nilai? [Y/N] : ");
            Scanner inputUlang = new Scanner(System.in);
            reload = inputUlang.nextLine();
        } while (reload.toLowerCase().equals("y") || reload.toLowerCase().equals("yes"));
    }
    
}
